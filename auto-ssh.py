#!/usr/bin/python

import os
from config import sshIP

print ""
for i, sshIPNew in enumerate([row[0] for row in sshIP], 1):
    print(" %d. %s" % (i, sshIPNew))

currentSSH = raw_input("\nTo quit hit x and enter \nPlease select SSH Server: ")

def sshServer():
    try:
        if currentSSH == "x":
            quit()
        else:
            print "SSH into", sshIP[int(currentSSH)-1][0], sshIP[int(currentSSH)-1][1]
            os.system("ssh root@" + sshIP[int(currentSSH)-1][1])
    except:
        print "Not in range!"

sshServer()
